using Gtk 4.0;
using Adw 1;

template $UpscalerWindow: Adw.ApplicationWindow {
  default-width: 700;
  default-height: 600;
  width-request: 300;
  height-request: 300;
  title: _("Upscaler");

  Adw.ToastOverlay toast {
    Overlay {
      [overlay]
      Revealer drag_revealer {
        can-target: false;
        transition-type: crossfade;

        child: Adw.StatusPage {
          icon-name: "folder-download-symbolic";
          title: _("Drop to Open");

          styles [
            "drag-overlay-status-page",
          ]
        };
      }

      child: Adw.NavigationView main_nav_view {
        Adw.NavigationPage {
          tag: "welcome-and-queue";
          title: _("Upscaler");

          child: WindowHandle {
            Adw.ToolbarView main_toolbar_view {
              [top]
              Adw.HeaderBar main_view_header_bar {
                show-title: false;

                [start]
                Button button_open {
                  action-name: "app.open";
                  visible: false;

                  child: Adw.ButtonContent {
                    icon-name: "document-open-symbolic";
                    label: _("Open");
                  };
                }

                [end]
                MenuButton {
                  icon-name: "open-menu-symbolic";
                  menu-model: primary_menu;
                  tooltip-text: _("Main Menu");
                  primary: true;
                }
              }

              content: Stack stack_upscaler {
                transition-type: crossfade;

                StackPage {
                  name: "stack_welcome_page";

                  child: Adw.StatusPage status_welcome {
                    title: _("Upscaler");
                    description: _("Drag and drop images here");
                    hexpand: true;
                    vexpand: true;

                    child: Box {
                      orientation: vertical;
                      spacing: 12;

                      Button button_input {
                        valign: center;
                        halign: center;
                        label: _("_Open File…");
                        use-underline: true;

                        styles [
                          "suggested-action",
                          "pill",
                        ]
                      }
                    };
                  };
                }

                StackPage {
                  name: "stack_queue_page";

                  child: Adw.PreferencesPage {
                    Adw.PreferencesGroup progress_group {
                      title: _("In Progress");

                      ListBox progress_list_box {
                        styles [
                          "boxed-list",
                        ]
                      }
                    }

                    Adw.PreferencesGroup queue_group {
                      title: _("Queued");

                      ListBox queue_list_box {
                        styles [
                          "boxed-list",
                        ]
                      }
                    }

                    Adw.PreferencesGroup completed_group {
                      visible: false;
                      title: _("Completed");

                      ListBox completed_list_box {
                        styles [
                          "boxed-list",
                        ]
                      }
                    }
                  };
                }

                StackPage {
                  name: "stack_loading";

                  child: Adw.Bin {
                    child: Adw.Spinner loading_page_spinner {
                      valign: center;
                      height-request: 32;
                      width-request: 32;
                    };
                  };
                }

              };
            }
          };
        }

        Adw.NavigationPage {
          title: _("Options");
          tag: "upscaling-options";

          child: Adw.ToolbarView {
            [top]
            Adw.HeaderBar {}

            content: Adw.PreferencesPage options_page {
              valign: center;

              Adw.PreferencesGroup {
                Stack stack_picture {
                  transition-type: crossfade;

                  StackPage {
                    name: "stack_picture";

                    child: Box {
                      orientation: vertical;
                      spacing: 24;

                      Picture image {
                        accessible-role: presentation;
                        halign: center;
                        height-request: 64;
                        width-request: 64;
                      }

                      Box {
                        orientation: vertical;
                        spacing: 9;

                        Label scaling_factor_title {
                          label: _("Image Scale");

                          styles [
                            "title-3",
                          ]
                        }

                        CenterBox {
                          start-widget: Box {
                            orientation: vertical;
                            hexpand: true;
                            margin-start: 12;
                            margin-end: 12;

                            Label {
                              label: _("Original Size");
                              halign: end;

                              styles [
                                "dim-label",
                                "title-4",
                                "font-size-smaller",
                              ]
                            }

                            $UpscalerDimensionLabel input_dimension {
                              halign: end;

                              styles [
                                "title-4",
                              ]
                            }
                          };

                          center-widget: Image {
                            icon-name: "arrow1-right-symbolic";
                            pixel-size: 24;
                            accessible-role: presentation;

                            styles [
                              "flip-on-rtl",
                            ]
                          };

                          end-widget: Box {
                            orientation: vertical;
                            hexpand: true;
                            margin-start: 12;
                            margin-end: 12;

                            Label {
                              label: _("Output Size");
                              halign: start;

                              styles [
                                "dim-label",
                                "heading",
                                "font-size-smaller",
                              ]
                            }

                            $UpscalerDimensionLabel output_dimension {
                              halign: start;

                              styles [
                                "title-4",
                              ]
                            }
                          };
                        }

                        $UpscalerScaleSpinButton spin_scale {
                          halign: center;

                          accessibility {
                            labelled-by: scaling_factor_title;
                            described-by: scaling_factor_title;
                          }

                          styles [
                            "title-4",
                          ]

                          adjustment: Adjustment {
                            lower: 2;
                            upper: 4;
                            value: 2;
                            step-increment: 1;
                            page-increment: 10;
                          };
                        }

                      }
                    };
                  }

                  StackPage {
                    name: "stack_loading";

                    child: Adw.Bin {
                      child: Adw.Spinner loading_image_loading {
                        valign: center;
                        height-request: 32;
                        width-request: 32;
                      };
                    };
                  }
                }

              }

              Adw.PreferencesGroup {
                Adw.ComboRow combo_models {
                  title: _("Type of Image");

                  model: StringList string_models {};
                }

                Adw.ActionRow {
                  title: _("Save Location");

                  Button button_output {
                    valign: center;
                    tooltip-text: _("Select Output Location");

                    Box {
                      spacing: 6;

                      Image {
                        icon-name: "document-open-symbolic";

                        accessibility {
                          labelled-by: button_output;
                        }
                      }

                      Label label_output {
                        label: _("(None)");
                      }
                    }
                  }
                }
              }

              Adw.PreferencesGroup {
                Button button_upscale {
                  valign: center;
                  halign: center;
                  label: _("_Upscale");
                  tooltip-text: _("Missing Save Location");
                  use-underline: true;
                  sensitive: false;

                  styles [
                    "suggested-action",
                    "pill",
                  ]
                }
              }
            };
          };
        }
      };
    }
  }
}

menu primary_menu {
  section {
    // item {
    //   label: _("Preferences");
    //   action: "app.preferences";
    // }
    item {
      label: _("_Keyboard Shortcuts");
      action: "win.show-help-overlay";
    }

    item {
      label: _("_About Upscaler");
      action: "app.about";
    }
  }
}
