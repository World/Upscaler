# scale_spin_button.py: Spin button for setting factors.
#
# Copyright (C) 2024 Upscaler Contributors
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# SPDX-License-Identifier: GPL-3.0-only

import logging

from typing import Any, Optional

from gi.repository import Gtk, GObject, Gdk


@Gtk.Template(
    resource_path="/io/gitlab/theevilskeleton/Upscaler/gtk/scale-spin-button.ui"
)
class UpscalerScaleSpinButton(Gtk.Box):
    __gtype_name__ = "UpscalerScaleSpinButton"

    factor: Gtk.Text = Gtk.Template.Child()  # type: ignore
    decrement: Gtk.Button = Gtk.Template.Child()  # type: ignore
    increment: Gtk.Button = Gtk.Template.Child()  # type: ignore

    def __init__(
        self, adjustment: Optional[Gtk.Adjustment] = None, **kwargs: Any
    ) -> None:
        super().__init__(**kwargs)

        setattr(self, "adjustment", adjustment)
        setattr(self, "grab_focus", self.__grab_focus)

        self.decrement.connect("clicked", self.__decrement_button_clicked_cb)
        self.increment.connect("clicked", self.__increment_button_clicked_cb)
        self.factor.connect("activate", self.__enter_clicked_cb)
        self.factor.connect("notify::has-focus", self.__has_focus_cb)

        self.get_property("adjustment")
        self.set_property("adjustment", self.adjustment)

        event_controller_key = Gtk.EventControllerKey.new()
        event_controller_key.connect("key-pressed", self.__key_pressed_cb)
        self.factor.add_controller(event_controller_key)

    @GObject.Property(type=Gtk.Adjustment)
    def adjustment(self) -> Optional[Gtk.Adjustment]:
        """Get or set the adjustment that holds the value of the spin button."""
        return self._adjustment

    @adjustment.setter  # type: ignore [no-redef]
    def adjustment(self, adjustment: Gtk.Adjustment) -> None:
        self._adjustment = adjustment

        if not isinstance(adjustment, Gtk.Adjustment):
            return

        text = str(self.get_value())

        self.update_property(
            (Gtk.AccessibleProperty.VALUE_MAX,),
            (self.adjustment.get_upper(),),
        )

        self.update_property(
            (Gtk.AccessibleProperty.VALUE_MIN,),
            (self.adjustment.get_lower(),),
        )

        self.factor.set_text(text)
        self.__update_widgets()

    def get_value(self) -> int:
        """Return the current value as an integer."""
        return int(self.adjustment.get_value())

    def __grab_focus(self, *args: Any) -> bool:
        if not (root := self.get_root()):
            return False

        root.set_focus(self.factor)
        return True

    def __key_pressed_cb(self, _: Gtk.EventControllerKey, key: int, *args: Any) -> bool:
        value = self.adjustment.get_value()
        is_greater_than_lower = value > self.adjustment.get_lower()
        is_smaller_than_upper = value < self.adjustment.get_upper()

        match key:
            case Gdk.KEY_Down if is_greater_than_lower:
                self.adjustment.set_value(value - self.adjustment.get_step_increment())
            case Gdk.KEY_Up if is_smaller_than_upper:
                self.adjustment.set_value(value + self.adjustment.get_step_increment())
            case Gdk.KEY_Page_Down if is_greater_than_lower:
                self.adjustment.set_value(value - self.adjustment.get_page_increment())
            case Gdk.KEY_Page_Up if is_smaller_than_upper:
                self.adjustment.set_value(value + self.adjustment.get_page_increment())
            case Gdk.KEY_Up | Gdk.KEY_Page_Up:
                self.keynav_failed(Gtk.DirectionType.UP)
                return True
            case Gdk.KEY_Down | Gdk.KEY_Page_Down:
                self.keynav_failed(Gtk.DirectionType.DOWN)
                return True
            case _:
                return False

        self.__update_widgets()

        return True

    def __has_focus_cb(self, _: Gtk.Text, *args: Any) -> None:
        if self.factor.has_focus():
            return

        self.__enter_clicked_cb()

    def __decrement_button_clicked_cb(self, *args: Any) -> None:
        if (value := self.adjustment.get_value()) > self.adjustment.get_lower():
            self.adjustment.set_value(value - self.adjustment.get_step_increment())

        self.__update_widgets()

    def __increment_button_clicked_cb(self, *args: Any) -> None:
        if (value := self.adjustment.get_value()) < self.adjustment.get_upper():
            self.adjustment.set_value(value + self.adjustment.get_step_increment())

        self.__update_widgets()

    def __enter_clicked_cb(self, *args: Any) -> None:
        text = self.factor.get_text()
        value: int

        if not text.isnumeric():
            self.factor.set_text(str(self.adjustment.get_value()))
            return

        value = int(text)
        if value < (lower := self.adjustment.get_lower()):
            self.adjustment.set_value(lower)
            self.__update_widgets()
            return

        elif value > (upper := self.adjustment.get_upper()):
            self.adjustment.set_value(upper)
            self.__update_widgets()
            return

        self.adjustment.set_value(value)
        self.__update_widgets()

    def __update_widgets(self, *args: Any) -> None:
        value = self.adjustment.get_value()
        text: str

        self.update_property(
            (Gtk.AccessibleProperty.VALUE_NOW,),
            (value,),
        )

        self.decrement.set_sensitive(value != self.adjustment.get_lower())
        self.increment.set_sensitive(value != self.adjustment.get_upper())

        text = str(value)
        self.factor.get_buffer().set_text(text, self.factor.get_max_length())

        logging.info(f"Scale value: {text}")
